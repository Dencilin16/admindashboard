import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { SidenavComponent } from './modules/admin/components/sidenav/sidenav.component';
import { HeaderComponent } from './modules/admin/components/header/header.component';
import { HomeComponent } from './modules/admin/components/home/home.component';
import { DashboardComponent } from './modules/admin/components/dashboard/dashboard.component';
import { MainpageComponent } from './modules/admin/components/mainpage/mainpage.component';
import { ApiService } from './modules/admin/components/services/api.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
  ],
  imports: [
    FormsModule,
    AppRoutingModule,
    BrowserModule,   
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,                                             
  ],
  providers: [ApiService],
  bootstrap: [AppComponent],
})
export class AppModule {}
