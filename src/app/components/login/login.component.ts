import { Component, OnInit } from '@angular/core';
import {FormGroup , FormBuilder } from '@angular/forms'
import { Router } from '@angular/router';
import {LoginserviceService} from './loginservice.service';
import { Validators } from '@angular/forms';
import { RegisterModel } from '../signup/data';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  currentUser!: RegisterModel;

  
  public Name!: string; 
  public loginForm !:FormGroup
  constructor(private FormBuilder : FormBuilder ,  private router:Router,private loginService:LoginserviceService ) { }

  ngOnInit(): void {
    this.loginForm = this.FormBuilder.group({
      email : ['',Validators.required],
      password: ['',Validators.required],
      fullname:['']  
    })
  }
 
  login(){
  
    let curr;
     // curr=this.currentUser.fullname;
    this.loginService.loginservice()
    .subscribe(res=>{
      const user= res.find((a:any)=>{
    
        return a.email=== this.loginForm.value.email && a.password === this.loginForm.value.password
      });
      if(user){
        alert("Login SucessFully");
       // alert("curr");
        this.loginForm.reset();
        this.router.navigate(['admin'])
      }else{
        alert("User Not Found");
      }
    },err=>{
      alert("something went wrong");
    })
  }
}
