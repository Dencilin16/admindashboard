export interface RegisterModel {
    fullname:string;
    email:string;
    password:string;
    mobile:number;
  }