import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormGroup,FormBuilder, Validators} from '@angular/forms'
import { Router } from '@angular/router';
import {SignupserviceService} from './signupservice.service';
import {RegisterModel} from './data';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  
  public signupForm !:FormGroup;

  constructor(private formBuilder:FormBuilder, private http:HttpClient,private router:Router,private serve:SignupserviceService) { }

  ngOnInit(): void {
    this.signupForm=this.formBuilder.group({
      fullname:['',Validators.required],
      email:['',Validators.required],
      password:['',Validators.required],
      mobile:['',Validators.required]
    })
  }
  signUp(){
     
      if (this.signupForm.value){
        const data=this.signupForm.value;
        this.serve.sign(data)
        .subscribe(res=>{
          alert("signup sucessfull");
          this.signupForm.reset();
          this.router.navigate(['login']);
        },err=>{
          alert("Something Went Wrong")
        })
      }
  
  }

}
