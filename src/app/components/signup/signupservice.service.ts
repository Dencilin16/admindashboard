import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SignupserviceService {

  constructor(private http:HttpClient) { }

  sign(data: any):Observable<object>{
    return this.http.post<any>("http://localhost:3000/signupUsers", data);
  }
}
