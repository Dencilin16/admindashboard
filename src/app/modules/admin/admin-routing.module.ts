import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { CreateUpdateEmployeeComponent } from './components/create-update-employee/create-update-employee.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EmployeeDialogComponent } from './components/employee-dialog/employee-dialog.component';
import { ExternalPromoterDetailsUpdateComponent } from './components/external-promoter-details-update/external-promoter-details-update.component';
import { ExternalPromoterDetailsComponent } from './components/external-promoter-details/external-promoter-details.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { MainpageComponent } from './components/mainpage/mainpage.component';
import { PartnertableComponent } from './components/partner-table/partner-table.component';
import { PartnerUpdatetableComponent } from './components/partner-update-table/partner-update-table.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { StudentTableComponent } from './components/student-table/student-table.component';
import { StudentUpdateTableComponent } from './components/student-update-table/student-update-table.component';
import { TrainerUpdatetableComponent } from './components/trainer-details-update/trainer-details-update.component';
import { TrainerDetailsComponent } from './components/trainer-details/trainer-details.component';

const routes: Routes = [

  { path: '', component:MainpageComponent ,
  children:[
    { path: 'create-update-employee', component: CreateUpdateEmployeeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'dashboard', component: DashboardComponent },
    {path:'EmployeeDialogComponent',component:EmployeeDialogComponent},
    {path:'partner-update-table',component:PartnerUpdatetableComponent},
    {path:'PartnertableComponent',component:PartnertableComponent},
    {path:'StudentTableComponent',component:StudentTableComponent},
    {path:'StudentUpdate',component:StudentUpdateTableComponent},
    {path:'ExternalPromoterDetailsComponent',component:ExternalPromoterDetailsComponent},
    {path:'ExternalPromoterUpdate',component:ExternalPromoterDetailsUpdateComponent},
    {path:'TrainerDetailsComponent',component:TrainerDetailsComponent},
    {path:'TrainerUpdatetableComponent',component:TrainerUpdatetableComponent}, 
    {path:'sidenav',component:SidenavComponent},
    {path:'',redirectTo:'admin/home',pathMatch:'full'}
  ],
  } 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
