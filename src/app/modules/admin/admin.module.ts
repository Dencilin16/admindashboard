import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { CreateUpdateEmployeeComponent } from './components/create-update-employee/create-update-employee.component';
import { EmployeeDialogComponent } from './components/employee-dialog/employee-dialog.component';
import { ExternalPromoterDetailsComponent } from './components/external-promoter-details/external-promoter-details.component';
import { ExternalPromoterDetailsUpdateComponent } from './components/external-promoter-details-update/external-promoter-details-update.component';
import { PartnertableComponent } from './components/partner-table/partner-table.component';
import { PartnerUpdatetableComponent } from './components/partner-update-table/partner-update-table.component';
import { StudentTableComponent } from './components/student-table/student-table.component';
import { StudentUpdateTableComponent } from './components/student-update-table/student-update-table.component';
import { TrainerUpdatetableComponent } from './components/trainer-details-update/trainer-details-update.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatRadioModule } from '@angular/material/radio';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { TrainerDetailsComponent } from './components/trainer-details/trainer-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { MainpageComponent } from './components/mainpage/mainpage.component';


@NgModule({
  declarations: [
    AdminDashboardComponent,
    EmployeeDialogComponent,
    CreateUpdateEmployeeComponent,
    PartnerUpdatetableComponent,
    PartnertableComponent,
    StudentTableComponent,
    StudentUpdateTableComponent,
    ExternalPromoterDetailsComponent,
    ExternalPromoterDetailsUpdateComponent,
    TrainerDetailsComponent,
    TrainerUpdatetableComponent,
    SidenavComponent,
    HeaderComponent,
    DashboardComponent,
    SidenavComponent,
    HomeComponent,
    MainpageComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MatDialogModule,
    MatInputModule,
    MatSortModule,
    MatPaginatorModule,
    MatButtonModule,
    MatFormFieldModule,
    MatTableModule, 
    MatSidenavModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    MatRadioModule,
    MatCheckboxModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }
