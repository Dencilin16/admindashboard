import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-employee-dialog',
  templateUrl: './employee-dialog.component.html',
  styleUrls: ['./employee-dialog.component.css']
})
export class EmployeeDialogComponent implements OnInit {

  EmployeeForm !: FormGroup;
actionBtn : string="Save"
  constructor(private formBuilder:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public editData : any,
        private api :ApiService,
    private dialogRef :MatDialogRef<EmployeeDialogComponent>) { 

  }
  
  ngOnInit(): void {
    this.EmployeeForm=this.formBuilder.group({
    //  UniqueId : ['',Validators.required],
      EmployeeId : ['',Validators.required],
      EmployeeName : ['',Validators.required],
      Email : ['',Validators.required],
      Contact :['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      Designation : ['',Validators.required],
      Location : ['',Validators.required],
    })
    if(this.editData){
      this.actionBtn="Update";
     // this.EmployeeForm.controls['UniqueId'].setValue(this.editData.UniqueId);
      this.EmployeeForm.controls['EmployeeId'].setValue(this.editData.EmployeeId);
      this.EmployeeForm.controls['EmployeeName'].setValue(this.editData.EmployeeName);
      this.EmployeeForm.controls['Email'].setValue(this.editData.Email);
      this.EmployeeForm.controls['Contact'].setValue(this.editData.Contact);
      this.EmployeeForm.controls['Designation'].setValue(this.editData.Designation);
      this.EmployeeForm.controls['Location'].setValue(this.editData.Location);
    }
  }
  
  
  addEmployee(){
    if(!this.editData){
      if(this.EmployeeForm.valid) {
      
        this.api.postEmployee(this.EmployeeForm.value)
      .subscribe({
        next:(res)=>{
          alert("Employee added Successfully");
        this.api.postEmployee(this.EmployeeForm.value)
        this.EmployeeForm.reset();
        this.dialogRef.close('save');
        },
        error:()=>{
    alert("Error While adding the Employee")
        }
      })
      }
    }else{
      this.updateEmployee()
    }
  }
  updateEmployee(){
    if(this.EmployeeForm.valid)
  this.api.putEmployee(this.EmployeeForm.value,this.editData.id)
.subscribe({
  next:(res)=>{
    alert("Employee updated successfully");
    this.EmployeeForm.reset();
    this.dialogRef.close('update')
  },
  error:()=>{
    alert("Error while updating the record!");
  }
})
}

}
