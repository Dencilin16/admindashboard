import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalPromoterDetailsUpdateComponent } from './external-promoter-details-update.component';

describe('ExternalPromoterDetailsUpdateComponent', () => {
  let component: ExternalPromoterDetailsUpdateComponent;
  let fixture: ComponentFixture<ExternalPromoterDetailsUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExternalPromoterDetailsUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalPromoterDetailsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
