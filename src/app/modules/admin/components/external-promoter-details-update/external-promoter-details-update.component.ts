import { Component, OnInit,ViewChild } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { ApiService } from '../services/api.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ExternalPromoterDetailsComponent } from '../external-promoter-details/external-promoter-details.component';


@Component({
  selector: 'app-external-promoter-details-update',
  templateUrl: './external-promoter-details-update.component.html',
  styleUrls: ['./external-promoter-details-update.component.css']
})
export class ExternalPromoterDetailsUpdateComponent implements OnInit {

  displayedColumns: string[] = ['delete','Name','Designation', 'Email','InsititutionName','CurrentJob','CandidateReferredMonthlyWeeklyYearly','MobileNumber','action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  
  constructor(private api:ApiService,private dialog :MatDialog){
  
  }
  ngOnInit(): void {
    this.getAllexternal();
  }
  
  getAllexternal(){
    this.api.getexternal()
    .subscribe({
      next:(res)=>{
        this.dataSource=new MatTableDataSource(res);
        this.dataSource.paginator=this.paginator;
        this .dataSource.sort=this.sort;
      },
      error:(err)=>{
        alert("Error while fetching the records")
      }
    })
  }
  applyfilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    //const filternum = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    //this.dataSource.filter = filternum;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  editexternal(row:any){
    this.dialog.open(ExternalPromoterDetailsComponent,{
      width:'30%',
      data:row
    }).afterClosed().subscribe(val=>{
      if(val ==='update'){
        this.getAllexternal();
      }
    })
  }
  openDialog() {
    this.dialog.open(ExternalPromoterDetailsComponent, {
      width:'30%'
    }).afterClosed().subscribe(val=>{
      if(val ==='save'){
        this.getAllexternal();
      }
    })
  }
  deleteexternal(id:number){
this.api.deleteexternal(id)
.subscribe({
  next:(res)=>{
    alert("External promoter Deleted Successfully")
    this.getAllexternal();
  },
  error:()=>{
    alert("Error while deleting the External promoter!!")   
  }
})
  }
  
  
}
