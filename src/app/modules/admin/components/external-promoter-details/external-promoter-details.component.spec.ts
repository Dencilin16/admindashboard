import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalPromoterDetailsComponent } from './external-promoter-details.component';

describe('ExternalPromoterDetailsComponent', () => {
  let component: ExternalPromoterDetailsComponent;
  let fixture: ComponentFixture<ExternalPromoterDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExternalPromoterDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalPromoterDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
