import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import * as apiService from '../services/api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-external-promoter-details.Component',
  templateUrl: './external-promoter-details.Component.html',
  styleUrls: ['./external-promoter-details.Component.css']
})
export class ExternalPromoterDetailsComponent implements OnInit {
externalForm !: FormGroup;
actionBtn : string="Save"
  constructor(private formBuilder:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public editData : any,
        private api :apiService.ApiService,
    private dialogRef :MatDialogRef<ExternalPromoterDetailsComponent>) { 

  }
  

  ngOnInit(): void {
    this.externalForm=this.formBuilder.group({
     // UniqueID : ['',Validators.required],
     Name : ['',Validators.required],
     Designation : ['',Validators.required],
     Email :  ['',Validators.required],
     InsititutionName : ['',Validators.required],
     CurrentJob : ['',Validators.required],
     CandidateReferredMonthlyWeeklyYearly : ['',Validators.required],
     MobileNumber : ['',[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
    })
    if(this.editData){
      this.actionBtn="Update";
      //this.partnerlistForm.controls['UniqueID'].setValue(this.editData.UniqueID);
      this.externalForm.controls['Name'].setValue(this.editData.Name);
      this.externalForm.controls['Designation'].setValue(this.editData.Designation);
      this.externalForm.controls['Email'].setValue(this.editData.Email);
      this.externalForm.controls['InsititutionName'].setValue(this.editData.InsititutionName);
      this.externalForm.controls['CurrentJob'].setValue(this.editData.CurrentJob);
      this.externalForm.controls['CandidateReferredMonthlyWeeklyYearly'].setValue(this.editData.CandidateReferredMonthlyWeeklyYearly);
      this.externalForm.controls['MobileNumber'].setValue(this.editData.MobileNumber);
      //this.partnerlistForm.controls['DepartmentName'].setValue(this.editData.DepartmentName);
    }
  }
  addexternal(){
    if(!this.editData){
      if(this.externalForm.valid) {
      
        this.api.postexternal(this.externalForm.value)
      .subscribe({
        next:(res)=>{
          alert("External Promoter added Successfully");
        this.api.postexternal(this.externalForm.value)
        this.externalForm.reset();
        this.dialogRef.close('save');
        },
        error:()=>{
    alert("Error While adding the external promoter")
        }
      })
      }
    }else{
      this.updateexternal()
    }
  }
  updateexternal(){
  if(this.externalForm.valid)
  this.api.putexternal(this.externalForm.value,this.editData.id)
.subscribe({
  next:(res)=>{
    alert("External Promoter updated successfully");
    this.externalForm.reset();
    this.dialogRef.close('update')
  },
  error:()=>{
    alert("Error while updating the record!");
  }
})
}
}
