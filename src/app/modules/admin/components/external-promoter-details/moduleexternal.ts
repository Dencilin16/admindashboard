export interface part {
    Name: string;
    Designation: string;
    Email: string;
    InsititutionName : string;
    CurrentJob : string;
    CandidateReferredMonthlyWeeklyYearly : string;
    MobileNumber : string;
  }