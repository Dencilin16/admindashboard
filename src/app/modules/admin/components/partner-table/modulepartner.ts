export interface part {
    UniqueId: number;
    PartnerName: string;
    CourseName: string;
    Institution: string;
    Email: string;
    Contact: number;
    ModeofTraining: string;
    Location: string;
  }