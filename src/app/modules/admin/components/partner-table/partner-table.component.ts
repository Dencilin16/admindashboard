import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';

import * as apiService from '../services/api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-partner-table.Component',
  templateUrl: './partner-table.Component.html',
  styleUrls: ['./partner-table.Component.css']
})
export class PartnertableComponent implements OnInit {
partnerlistForm !: FormGroup;
actionBtn : string="Save"
  constructor(private formBuilder:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public editData : any,
        private api :apiService.ApiService,
    private dialogRef :MatDialogRef<PartnertableComponent>) { 

  }

  ngOnInit(): void {
    this.partnerlistForm=this.formBuilder.group({
      
      PartnerName : ['',Validators.required],
      CourseName : ['',Validators.required],
      Institution : ['',Validators.required],
      Email : ['',Validators.required],
      Contact :['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      
      
      ModeofTraining : ['',Validators.required],
      Location : ['',Validators.required],
    })
    if(this.editData){
      this.actionBtn="Update";
      this.partnerlistForm.controls['id'].setValue(this.editData.id);
      this.partnerlistForm.controls['PartnerName'].setValue(this.editData.PartnerName);
      this.partnerlistForm.controls['CourseName'].setValue(this.editData.CourseName);
      this.partnerlistForm.controls['Institution'].setValue(this.editData.Institution);
      this.partnerlistForm.controls['Email'].setValue(this.editData.Email);
      this.partnerlistForm.controls['Contact'].setValue(this.editData.Contact);
      this.partnerlistForm.controls['ModeofTraining'].setValue(this.editData.ModeofTraining);
      this.partnerlistForm.controls['Location'].setValue(this.editData.Location);
   
    }
  }
  addpartner(){
    if(!this.editData){
      if(this.partnerlistForm.valid) {
      
        this.api.postpartner(this.partnerlistForm.value)
      .subscribe({
        next:(res)=>{
          alert("PartnerList added Successfully");
        this.api.postpartner(this.partnerlistForm.value)
        this.partnerlistForm.reset();
        this.dialogRef.close('save');
        },
        error:()=>{
    alert("Error While adding the Department")
        }
      })
      }
    }else{
      this.updatepartner()
    }
  }
  updatepartner(){
    if(this.partnerlistForm.valid)
  this.api.putpartner(this.partnerlistForm.value,this.editData.id)
.subscribe({
  next:(res)=>{
    alert("PartnerList updated successfully");
    this.partnerlistForm.reset();
    this.dialogRef.close('update')
  },
  error:()=>{
    alert("Error while updating the record!");
  }
})
}
}
