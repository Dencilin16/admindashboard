import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerUpdateTableComponent } from './partner-update-table.component';

describe('PartnerUpdateTableComponent', () => {
  let component: PartnerUpdateTableComponent;
  let fixture: ComponentFixture<PartnerUpdateTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerUpdateTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerUpdateTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
