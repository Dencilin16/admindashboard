import { Component, OnInit,ViewChild } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { ApiService } from '../services/api.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { PartnertableComponent } from '../partner-table/partner-table.component';
import { MatCheckboxModule } from '@angular/material/checkbox';


@Component({
  selector: 'app-partner-update-table',
  templateUrl: './partner-update-table.component.html',
  styleUrls: ['./partner-update-table.component.css']
})
export class PartnerUpdatetableComponent implements OnInit {

  displayedColumns: string[] = ['delete','PartnerName', 'CourseName','Institution','Email','Contact','ModeofTraining','Location','action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private api:ApiService,private dialog :MatDialog){
  
  }
  ngOnInit(): void {
    this.getAllpartner();
  }
  getAllpartner(){
    this.api.getpartner()
    .subscribe({
      next:(res)=>{
        this.dataSource=new MatTableDataSource(res);
        this.dataSource.paginator=this.paginator;
        this .dataSource.sort=this.sort;
      },
      error:(err)=>{
        alert("Error while fetching the records")
      }
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  editpartner(row:any){
    this.dialog.open(PartnertableComponent,{
      width:'30%',
      data:row
    }).afterClosed().subscribe(val=>{
      if(val ==='update'){
        this.getAllpartner();
      }
    })
  }
  openDialog() {
    this.dialog.open(PartnertableComponent, {
      width:'30%'
    }).afterClosed().subscribe(val=>{
      if(val ==='save'){
        this.getAllpartner();
      }
    })
  }
  deletepartner(id:number){
this.api.deletepartner(id)
.subscribe({
  next:(res)=>{
    alert("Department Deleted Successfully")
    this.getAllpartner();
  },
  error:()=>{
    alert("Error while deleting the Department!!")   
  }
})
  }
  
  
}
