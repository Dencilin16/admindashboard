

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
providedIn: 'root'
})
export class ApiService
{
 constructor(private http : HttpClient) { }
//   postDepartment(data: any):Observable<any>{
//   return this.http.post<any>("http://localhost:3000/posts/",data);
//   }
//   getDepartment():Observable<any>{
//   return this.http.get<any>("http://localhost:3000/posts/"); }
//   putDepartment(data:any,id:number):Observable<any>{
//   return this.http.put<any>("http://localhost:3000/posts/"+id,data);
//  }
//  deleteDepartment(id: number): Observable<any> {
//   return this.http.delete<any>("http://localhost:3000/posts/"+id);
//   }
postEmployee(data: any):Observable<any>{
 return this.http.post<any>("http://localhost:3000/comments/",data);
 }
getEmployee():Observable<any>{
return this.http.get<any>("http://localhost:3000/comments/");
}
putEmployee(data:any,id:number):Observable<any>{
return this.http.put<any>("http://localhost:3000/comments/"+id,data);
}
deleteEmployee(id: number): Observable<any> {
return this.http.delete<any>("http://localhost:3000/comments/"+id);

}
    postpartner(data: any):Observable<any>{
    return this.http.post<any>("http://localhost:3000/posts/",data);
    }
    getpartner():Observable<any>{
    return this.http.get<any>("http://localhost:3000/posts/");
    }
    putpartner(data:any,id:number):Observable<any>{
    return this.http.put<any>("http://localhost:3000/posts/"+id,data);
    }
    deletepartner(id: number): Observable<any> {
    return this.http.delete<any>("http://localhost:3000/posts/"+id);

    
    }
    poststudent(data: any):Observable<any>{
        return this.http.post<any>("http://localhost:3000/post/",data);
        }
        getstudent():Observable<any>{
        return this.http.get<any>("http://localhost:3000/post/");
        }
        putstudent(data:any,id:number):Observable<any>{
        return this.http.put<any>("http://localhost:3000/post/"+id,data);
        }
        deletestudent(id: number): Observable<any> {
        return this.http.delete<any>("http://localhost:3000/post/"+id);
}
postexternal(data: any):Observable<any>{
    return this.http.post<any>("http://localhost:3000/external/",data);
    }
    getexternal():Observable<any>{
    return this.http.get<any>("http://localhost:3000/external/");
    }
    putexternal(data:any,id:number):Observable<any>{
    return this.http.put<any>("http://localhost:3000/external/"+id,data);
    }
    deleteexternal(id: number): Observable<any> {
    return this.http.delete<any>("http://localhost:3000/external/"+id);
    }
    postTrainer(data: any):Observable<any>{
        return this.http.post<any>("http://localhost:3000/trainer/",data);
        }
        getTrainer():Observable<any>{
        return this.http.get<any>("http://localhost:3000/trainer/");
        }
        putTrainer(data:any,id:number):Observable<any>{
        return this.http.put<any>("http://localhost:3000/trainer/"+id,data);
        }
        deleteTrainer(id: number): Observable<any> {
        return this.http.delete<any>("http://localhost:3000/trainer/"+id);
        }
}
