export interface student{
    uniqueid : number;
    studentname : string;
    coursename : string ;
    email : string ;
    contact : string ;
    modeoflearning : string ;
    location : string ;
    payment : string ;
}