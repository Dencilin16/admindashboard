import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';

import * as apiService from '../services/api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-student-table.Component',
  templateUrl: './student-table.Component.html',
  styleUrls: ['./student-table.Component.css']
})
export class StudentTableComponent implements OnInit {
studentForm !: FormGroup;
actionBtn : string="Save"
  constructor(private formBuilder:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public editData : any,
        private api :apiService.ApiService,
    private dialogRef :MatDialogRef<StudentTableComponent>) { 

  }

  ngOnInit(): void {
    this.studentForm=this.formBuilder.group({
     
      studentname : ['',Validators.required],
      coursename :['',Validators.required],
      email:['',Validators.required],
      contact:['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      modeoflearning:['',Validators.required],
      location:['',Validators.required],
      payment:['',Validators.required],



    })
    if(this.editData){
      this.actionBtn="Update";
      this.studentForm.controls['id'].setValue(this.editData.id);
      this.studentForm.controls['studentname'].setValue(this.editData.studentname);
      this.studentForm.controls['coursename'].setValue(this.editData.coursename);
      this.studentForm.controls['email'].setValue(this.editData.email);
      this.studentForm.controls['contact'].setValue(this.editData.contact);
      this.studentForm.controls['modeoflearning'].setValue(this.editData.modeoflearning);
      this.studentForm.controls['location'].setValue(this.editData.location);
      this.studentForm.controls['payment'].setValue(this.editData.payment);
    }
  }
  addstudent(){
    if(!this.editData){
      if(this.studentForm.valid) {
      
        this.api.poststudent(this.studentForm.value)
      .subscribe({
        next:(res)=>{
          alert("Student added Successfully");
        this.api.poststudent(this.studentForm.value)
        this.studentForm.reset();
        this.dialogRef.close('save');
        },
        error:()=>{
    alert("Error While adding the Student")
        }
      })
      }
    }else{
      this.updatestudent()
    }
  }
  updatestudent(){
    if(this.studentForm.valid)
  this.api.putstudent(this.studentForm.value,this.editData.id)
.subscribe({
  next:(res)=>{
    alert("Student updated successfully");
    this.studentForm.reset();
    this.dialogRef.close('update')
  },
  error:()=>{
    alert("Error while updating the record!");
  }
})
}
}

