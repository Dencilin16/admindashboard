import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentUpdateTableComponent } from './student-update-table.component';

describe('StudentUpdateTableComponent', () => {
  let component: StudentUpdateTableComponent;
  let fixture: ComponentFixture<StudentUpdateTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentUpdateTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StudentUpdateTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
