import { Component, OnInit,ViewChild } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { ApiService } from '../services/api.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { StudentTableComponent } from '../student-table/student-table.component';
import { MatCheckbox } from '@angular/material/checkbox';

@Component({
  selector: 'app-student-update-table',
  templateUrl: './student-update-table.component.html',
  styleUrls: ['./student-update-table.component.css']
})
export class StudentUpdateTableComponent implements OnInit {

  displayedColumns: string[] = ['delete', 'studentname','coursename','email','contact','modeOflearning','location','payment','action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private api:ApiService,private dialog :MatDialog){
  
  }
  ngOnInit(): void {
    this.getAllstudent();
  }
  getAllstudent(){
    this.api.getstudent()
    .subscribe({
      next:(res)=>{
        this.dataSource=new MatTableDataSource(res);
        this.dataSource.paginator=this.paginator;
        this .dataSource.sort=this.sort;
      },
      error:(err)=>{
        alert("Error while fetching the records")
      }
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  editstudent(row:any){
    this.dialog.open(StudentTableComponent,{
      width:'30%',
      data:row
    }).afterClosed().subscribe(val=>{
      if(val ==='update'){
        this.getAllstudent();
      }
    })
  }
  openDialog() {
    this.dialog.open(StudentTableComponent, {
      width:'30%'
    }).afterClosed().subscribe(val=>{
      if(val ==='save'){
        this.getAllstudent();
      }
    })
  }
  deletestudent(id:number){
this.api.deletestudent(id)
.subscribe({
  next:(res)=>{
    alert("student Deleted Successfully")
    this.getAllstudent();
  },
  error:()=>{
    alert("Error while deleting the student!!")   
  }
})
  } 
}
