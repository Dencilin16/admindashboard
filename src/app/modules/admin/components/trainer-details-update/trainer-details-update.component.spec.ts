import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerDetailsUpdateComponent } from './trainer-details-update.component';

describe('TrainerDetailsUpdateComponent', () => {
  let component: TrainerDetailsUpdateComponent;
  let fixture: ComponentFixture<TrainerDetailsUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainerDetailsUpdateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TrainerDetailsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
