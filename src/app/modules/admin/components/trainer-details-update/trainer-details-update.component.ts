import { Component, OnInit,ViewChild } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { ApiService } from '../services/api.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { TrainerDetailsComponent } from '../trainer-details/trainer-details.component';

@Component({
  selector: 'app-trainer-details-update',
  templateUrl: './trainer-details-update.component.html',
  styleUrls: ['./trainer-details-update.component.css']
})
export class TrainerUpdatetableComponent implements OnInit {

  displayedColumns: string[] = ['delete','name', 'designation','employeeId','email','mode','trainingCourse','mobileNumber','action'];
  dataSource!: MatTableDataSource<any> ;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  tasks = [{row:'',completed:false}]
  
  constructor(private api:ApiService,private dialog :MatDialog){
  
  }
  ngOnInit(): void {
    this.getAllTrainers();
  }
  getAllTrainers(){
    this.api.getTrainer()
    .subscribe({
      next:(res)=>{
        this.dataSource=new MatTableDataSource(res);
        this.dataSource.paginator=this.paginator;
        this .dataSource.sort=this.sort;
      },
      error:(err)=>{
        alert("Error while fetching the records")
      }
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  editTrainer(row:any){
    this.dialog.open(TrainerDetailsComponent,{
      width:'30%',
      data:row
    }).afterClosed().subscribe(val=>{
      if(val ==='update'){
        this.getAllTrainers();
      }
    })
  }
  openDialog() {
    this.dialog.open(TrainerDetailsComponent, {
      width:'30%'
    }).afterClosed().subscribe(val=>{
      if(val ==='save'){
        this.getAllTrainers();
      }
    })
  }

  
  deleteTrainer(id:number){
this.api.deleteTrainer(id)
.subscribe({
  next:(res)=>{
    alert("Trainer Deleted Successfully")
    this.getAllTrainers();
  },
  error:()=>{
    alert("Error while deleting the Trainer!!")   
  }
})
  }
}

  
  

