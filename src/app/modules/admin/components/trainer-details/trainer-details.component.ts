import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '../services/api.service';
@Component({
  selector: 'app-trainer-details.Component',
  templateUrl:'./trainer-details.Component.html',
  styleUrls: ['./trainer-details.Component.css'],
})
export class TrainerDetailsComponent implements OnInit {
  trainerForm !: FormGroup;
actionBtn : string="Save"
  constructor(private formBuilder:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public editData : any,
        private api :ApiService,
    private dialogRef :MatDialogRef<TrainerDetailsComponent>,
    ) { 

  }

  ngOnInit(): void {
    this.trainerForm=this.formBuilder.group({
     
      name : ['',Validators.required],
      designation :['',Validators.required],
      employeeId:['',Validators.required],
      email:['',[Validators.required,Validators.email]] ,
      mode:['',Validators.required],
      trainingCourse:['',Validators.required],
      mobileNumber:['',[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      
     
    })
    if(this.editData){
      this.actionBtn="Update";
     // this.studentForm.controls['id'].setValue(this.editData.id);
      this.trainerForm.controls['name'].setValue(this.editData.name);
      this.trainerForm.controls['designation'].setValue(this.editData.designation);
      this.trainerForm.controls['employeeId'].setValue(this.editData.employeeId);
      this.trainerForm.controls['email'].setValue(this.editData.email);
      
      this.trainerForm.controls['mode'].setValue(this.editData.mode);
      this.trainerForm.controls['trainingCourse'].setValue(this.editData.trainingCourse);
      this.trainerForm.controls['mobileNumber'].setValue(this.editData.mobileNumber);
      
    }
  }
  addTrainer(){
    if(!this.editData){
      if(this.trainerForm.valid) {
      
        this.api.postTrainer(this.trainerForm.value)
      .subscribe({
        next:(res)=>{
          alert("Trainer added Successfully");
        this.api.postTrainer(this.trainerForm.value)
        this.trainerForm.reset();
        this.dialogRef.close('save');
        },
        error:()=>{
    alert("Error While adding the Student")
        }
      })
      }
    }else{
      this.updateTrainer()
    }
  }
  updateTrainer(){
  if(this.trainerForm.valid){
  this.api.putTrainer(this.trainerForm.value,this.editData.id)
.subscribe({
  next:(res)=>{
    alert("Trainer updated successfully");
    this.trainerForm.reset();
    this.dialogRef.close('update')
  },
  error:()=>{
    alert("Error while updating the record!");
  }
})
  }
}
}

