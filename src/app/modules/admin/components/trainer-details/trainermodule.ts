export interface trainer{
   
    name : string;
    designation : string ;
    employeeId : string  ;
    email : number ;
    mode : string ;
    trainingCourse : string ;
    mobileNumber : string ;
}